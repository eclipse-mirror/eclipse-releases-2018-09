#!/bin/bash

deploymvn() {
	path="$1"
	file=$(basename "$1")
	id=$(echo "$file" | sed 's/_.*//')
	version=$(echo "$file" | sed 's/^.*_//' | sed 's/\.jar//')
	mvn deploy:deploy-file -DgroupId=$id \
		-DartifactId=$id \
		-Dversion=$version \
		-Dpackaging=jar \
		-Dfile=$path \
		-DrepositoryId=gitlab-maven \
		-Durl=https://gitlab.com/api/v4/projects/CI_PROJECT_ID/packages/maven
}
export -f deploymvn

find -type f -wholename '*/plugins/*.jar' -exec bash -c 'deploymvn "$0"' {} \;
